import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/*
/Users/liuzy/code/bjxgj/img/child/IMG_2F932C52835C-1.jpeg /Users/liuzy/code/bjxgj/img/child/child.jpeg
/Users/liuzy/code/bjxgj/img/mama/IMG_33CF498026D4-1.jpeg /Users/liuzy/code/bjxgj/img/mama/mama.jpeg
/Users/liuzy/code/bjxgj/img/mama2/IMG_9BA300399D97-1.jpeg /Users/liuzy/code/bjxgj/img/mama2/mama2.jpeg
 */
public class Main {
    private static String[] ARGS;
    private static File src;
    private static File dst;

    // src dst time datetime
    public static void main(String[] args) {
        System.out.println("args: " + Arrays.toString(args));
        if (args.length < 2) {
            System.out.println("params error !");
            return;
        }
        ARGS = args;
        src = new File(args[0]);
        dst = new File(args[1]);
        if (!src.exists()) {
            System.out.println("src not found !" + src.getPath());
            return;
        }
        loadFont();
        if (args[0].contains("/baba2/") || args[0].contains("/mama2/")) {  // src包含 /baba2/ 时画行程码
            qrXCM();
        } else {
            qrJKM();
        }
    }

    public static void qrXCM() {
        try (FileOutputStream fos = new FileOutputStream(dst)) {
            Image img = ImageIO.read(src);
            int width = img.getWidth(null);
            int height = img.getHeight(null);
            System.out.printf("src => %d x %d\r\n", width, height);
            if (width != 750 || height != 1334) {
                System.out.println("src error must 750x1334 !");
            }

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, -3);
            calendar.add(Calendar.SECOND, (int) (Math.random() * (100 - 30 + 1) + 50) * -1);
            Date d = calendar.getTime();

            String time = ARGS.length > 2 && !ARGS[2].isEmpty() ? ARGS[2] : new SimpleDateFormat("HH:mm").format(d);
            String datetime = ARGS.length > 3 && !ARGS[3].isEmpty() ? ARGS[3] : new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").format(d);

            BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bi.createGraphics();
            g.drawImage(img, 0, 0, width, height, null);

            g.setColor(new Color(239, 239, 239)); // 背景
            g.fillRect(338, 8, 74, 26); // 填充系统时钟区域

            g.setColor(new Color(255, 255, 255)); // 背景白色
            g.fillRect(280, 520, 336, 42); // 填充日期时间区域

            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

            g.setColor(new Color(0, 0, 0)); // 黑色
            g.setFont(new Font("微软雅黑", Font.BOLD, 24));
            g.drawString(time, 340, 30); // 系统时钟

            g.setColor(new Color(0xa9, 0xa9, 0xb1));
            g.setFont(new Font("微软雅黑", Font.PLAIN, 34));
            g.drawString(datetime, 282, 552); // 日期时间

            g.dispose();
            ImageIO.write(bi, "png", fos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void qrJKM() {
        try (FileOutputStream fos = new FileOutputStream(dst)) {
            Image img = ImageIO.read(src);
            int width = img.getWidth(null);
            int height = img.getHeight(null);
            System.out.printf("src => %d x %d\r\n", width, height);
            if (width != 750 || height != 1334) {
                System.out.println("src error must 750x1334 !");
            }

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, -3);
            calendar.add(Calendar.SECOND, (int) (Math.random() * (100 - 30 + 1) + 30) * -1);
            Date d = calendar.getTime();

            String time = ARGS.length > 2 && !ARGS[2].isEmpty() ? ARGS[2] : new SimpleDateFormat("HH:mm").format(d);
            String datetime = ARGS.length > 3 && !ARGS[3].isEmpty() ? ARGS[3] : new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);

            BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bi.createGraphics();
            g.drawImage(img, 0, 0, width, height, null);

            g.setColor(new Color(200, 185, 144)); // 背景
            g.fillRect(338, 8, 74, 26); // 填充时钟区域

            g.setColor(new Color(255, 255, 255));
            g.fillRect(232, 660, 290, 38); // 填充日期时间区域

            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

            g.setFont(new Font("微软雅黑", Font.BOLD, 24));
            g.drawString(time, 340, 30); // 时钟

            g.setColor(new Color(1, 1, 1));
            g.setFont(new Font("微软雅黑", Font.PLAIN, 28));
            g.drawString(datetime.substring(0, datetime.length() - 2), 230, 690); // 日期
            g.setFont(new Font("微软雅黑", Font.PLAIN, 34));
            g.drawString(datetime.substring(datetime.length() - 2), 477, 690); // 秒

            g.dispose();
            ImageIO.write(bi, "png", fos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadFont() {
        try {
            Font msyh = Font.createFont(Font.TRUETYPE_FONT, new File("msyh.ttc"));
            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(msyh);
            Font msyhl = Font.createFont(Font.TRUETYPE_FONT, new File("msyhl.ttc"));
            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(msyhl);
        } catch (Exception e) {
            System.out.println("load msyh.ttc font error, " + e.getMessage());
        }
    }
}
