# 班级小管家

> 自动提交作业

- 基本流程
    - 所有作业`getParent`
    - 作业编号`_id`
    - 作业详情`check2dakaNew2`
    - 作业表单`invest`
    - 上传图片`uploadOss`
    - 提交表单`feedbackWithOss`
- 开始
  - 自行获取`conf.js`部分参数
  - 自行修改`invest`作业表单
  - 自行适配`qrcode`JAVA代码
- 配置文件
  - `appkey`管家在微信的应用标识
  - `openId`你在此公众号的用户标识
  - `memberId`你在管家的用户标识
  - `OSSAccessKeyId`阿里云对象存储相关
- 运行
  - `sh app.sh` 或 `node app.js`
